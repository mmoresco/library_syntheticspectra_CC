This library contains the synthetic spectra generated with different stellar population synthesis (SPS) models, stellar ages, stellar metallicities (Z) and initial mass functions (IMF) for the analysis discussed in Moresco et al. (2019).

The complete list of available parameters can be found in Tab. 1 of Moresco et al. (2019). The SPS models analyzed are the updated Bruzual and Charlot models (BC16), Maraston & Stromback (2011) models (M11), the models by Conroy et al. (2009) (FSPS), the model by Vazdekis et al. (2016) (E-MILES), and Bruzual and Charlot (2003) models (BC03), 

The folders with the name of the SPS models contain the spectra obtained from the different models, with the format spec\_"model"\_"stellar library"\_"IMF"\_"metallicity"\_"age"Gyr.dat.
The folder named D4000-age contains the D4000-age relations estimated from the analysis of the synthetic spectra discussed above.

If you use these data, please refer to Moresco et al. (2019).

